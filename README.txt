Proyecto Moviepedia por Juan Cruz Pereyra Carrillo

Se necesita tener DOCKER instalado

Para iniciar el proyecto deberemos abrir una terminal en el mismo directorio que se encuentra este archivo

Con docker utilizamos el comando


        sudo docker-compose build
        
seguido de

        sudo docker-compose up

        
Luego de esto podremos visualizar la página en 0.0.0.0:8000

Funcionamiento interno:

El código que se encuentra en Setup_Db.txt es el código que utilice para pasar los datos de los archivos .csv a la base de datos. Dicho código debe ser colocado en la shell de django (python manage.py shell) 

Django está funcionando junto a Django-RestFramework cuya API podremos visualizar entrando desde la navbar en donde dice "API"

Si queremos entrar al admin de django:
User: admin
Pass: admin

No opte por usar bases de datos relacionales debido a que quería mantener el proyecto simple para no alargarlo tanto. Aunque soy consiente de que podría haber creado una base de datos relacional conectando los títulos con sus actores, etc.


Para el frontend utilice React-Redux (tecnologías que tuve que aprender) y para conectar la API con react utilice Axios

Utilice Bootstrap para darle estilo a la página. Para no hacer más pesado el archivo utilicé  links como href.
