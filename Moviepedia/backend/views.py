from django.shortcuts import render
from .models import Movies
from .serializer import MovieSerializer
from rest_framework import generics
from rest_framework import filters

# Searchers

class MovieAPIView(generics.ListCreateAPIView):
	search_fields = ['title','genres']
	filter_backends = (filters.SearchFilter,)
	queryset = Movies.objects.all()
	serializer_class = MovieSerializer
