from django.db import models

# Create your models here.

class Movies(models.Model):
	#title.basics
	tconst = models.CharField(max_length=25, primary_key= True)
	titleType = models.CharField(max_length=10)
	title = models.CharField(max_length=50)
	startYear = models.CharField(max_length=30)
	endYear = models.CharField(max_length=4)
	runtimeMinutes = models.CharField(max_length=25)
	genres = models.TextField()
	averageRatting=models.FloatField()
	
	class Meta:
		db_table="Movies"

	def __str__(self):
		return self.title
	
		

class Rating(models.Model):
	#title.Rating
	tconst = models.CharField(max_length=25, primary_key=True)
	averageRatting = models.FloatField()
	numVotes = models.IntegerField()
	class Meta:
		db_table="Rating" 
	def __str__(self):
		return self.tconst