from django.urls import path
from rest_framework import routers
from .api import MovieViewSet
from .views import MovieAPIView



urlpatterns=[
	path('', MovieAPIView.as_view())

]