from django import forms
from .models import Movies

class Movie_Search_Form(forms.ModelForm):
	title= forms.CharField(label='¿Qué peli buscás?', widget= forms.TextInput(attrs={'placeholder':"Películas",
		'name':'query', 'id':'query'}))
	class Meta:
		model= Movies
		fields = [
			'title',]