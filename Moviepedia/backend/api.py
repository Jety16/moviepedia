from .models import Movies
from rest_framework import viewsets, permissions
from .serializer import MovieSerializer

#lead ViewSet
class MovieViewSet(viewsets.ReadOnlyModelViewSet):
	queryset= Movies.objects.all()
	permission_classes = [
		permissions.AllowAny
	]

	serializer_class= MovieSerializer

