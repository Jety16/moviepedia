import React, {Fragment, Component} from 'react';
import {connect} from 'react-redux';
//Components
import Searcher from './Searcher';
import Movies from './Movies';

export class Dashboard extends Component{
	
	render(){
		return(
			<Fragment >
				<h1 className="text-center" style={{margin: '2em 0 0 0'}}> MOVIEPEDIA </h1>
				<p className="text-center"> Filtra películas por genero y/o por nombre. Mira el año en que fueron publicadas y su "rating" de la mano de IMDB </p>
				<div className="container">
					<Searcher/>
					<Movies/>
				</div>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	movies:state.movies.movies,
});

export default connect(mapStateToProps) (Dashboard);