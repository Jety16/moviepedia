import React, {Component } from 'react';
import {connect} from 'react-redux';

export class MovieCard extends Component {
	render(){
          const {movie}=this.props

    return (
        <div className= "card card-body bg-light card-text" style={{width: "22em", margin: '15px', padding: '0',fontSize:'30px', boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"}}>

          <div className="card-header bg-dark" style = {{padding:'0 20px 10px 20px', position:""}}>
            <h6 className="text-light " style = {{fontSize:"14px", display:"inline-block"}} > {movie.title} </h6>
          </div>

          <h6 className = 'text-dark card-text bg-secondary' style={{ fontSize:"13px", padding: '10px 0px 0px 20px'}}>
                Genero: {movie.genres}</h6>

          <h6 className = 'text-dark card-text' style={{fontSize:"13px", padding: '10px 0px 0px 20px'}}>
                Año: {movie.startYear}</h6>

          <div>
            <svg xmlns="http://www.w3.org/2000/svg"  style={{width:"16", height:"16", fill:"currentColor", display:"inline", position:"relative", float:"right", margin:"0 20px 10px 0 "}}className="bi bi-star-fill" viewBox="0 0 16 16">
              <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
            </svg>
            <h6 className = 'text-dark card-text'style={{fontSize:"16px", display:"inline", position:"relative", float:"right"}}>{movie.averageRatting}</h6>
          </div>
        </div>
    )
	}
}

const mapStateToProps = state => ({
  movies:state.movies.movies,
});

export default connect(mapStateToProps) (MovieCard);