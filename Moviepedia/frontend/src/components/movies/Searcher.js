import React, {Component} from 'react';
import {searchMovie,fetchMovies} from '../../actions/search'
import {connect} from 'react-redux';
import Movies from './Movies';



export class Searcher extends Component {
	onChange = e => {
		this.props.searchMovie(e.target.value);
		if (e.target.value !== "" ){
			this.props.fetchMovies(this.props.text)
		}else {
			this.props.fetchMovies("####")
		}
	};

	onSubmit = e =>{
		e.preventDefault()
	};

	render(){
    	return (
			<form id="searchForm" onSubmit={this.onSubmit}  style={{margin:'3em 0 0 0'}}>
					<div className="container-lg" style= {{margin: '20px 0 20px 0'}} >
    					<input type="text" className="form-control text-lg bg-light"  aria-describedby="text" placeholder="¿Qué estás buscando?" onChange={this.onChange}></input>
    					<small className="form-text text-secondary"> Ej: The House Of Cards, Ej: Comedy </small>

 			 		</div>
 			</form>
		);
	}

}

const mapStateToProps = state => ({
	text:state.movies.text,
	movies:state.movies.movies,

});

export default connect(mapStateToProps, {searchMovie, fetchMovies}) (Searcher);