import React, {Component} from 'react';
import { connect } from 'react-redux';
import MovieCard from './MovieCard';
export class Movies extends Component {

	render(){
		const {movies} = this.props;
		let content = '';
		content = 	movies.length > 0 ? movies.map((movie) => <MovieCard key = {movie.tconst} movie = {movie} />): null;		
		return (
			<div className="row">
				{content}
			</div>
		)
	}
};


const mapStateToProps = state => ({
  movies:state.movies.movies,

});

export default connect(mapStateToProps) (Movies);