import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';

//redux
import {Provider} from 'react-redux';
//components

//header
import Header from './layout/Header';

//dashboard 
import Dashboard from './movies/Dashboard';

//Footer
import Footer from './layout/Footer'


import store from '../store';


class App extends Component{
	render() {
	  return(
	  	<Provider store={store}	>
	  		<>
         		<Header />
	  			<div className= "container">
	  				<Dashboard />
	  				<Footer/>
	  			</div>
	  			
	  		</>
	  	</Provider>
	  	)
	}
}

ReactDOM.render(<App />,document.getElementById('app'));