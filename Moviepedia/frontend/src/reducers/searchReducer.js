import {
	SEARCH_MOVIES,
	FETCH_MOVIES,
} from '../actions/types'

const initialState= {
	text:'',
	movies:[],
	rating: []
}
export default function(state = initialState, action) {
	switch (action.type){
		case SEARCH_MOVIES:
			return {
				...state,
				text:action.payload,
			};
		case FETCH_MOVIES:
			return {
				...state,
				movies:action.payload

			};
		default:
			return state;
	}
}