import {SEARCH_MOVIES, FETCH_MOVIES, FETCH_RATING} from './types';
import axios from 'axios';



export const searchMovie = text => dispatch => {
	dispatch({
		type:SEARCH_MOVIES,
		payload:text
	});
};

export const fetchMovies = text => dispatch =>{
	const queryString = require('query-string');
	axios.get('api/?' + queryString.stringify({'search': text}, {arrayFormat: '+'})). then(response => dispatch({
		type: FETCH_MOVIES,
		payload: response.data
	}))
	.catch(err => console.log(err))
};
